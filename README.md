# Progress Exemplo Documento MLA

Exemplo de customização do Aprovador para completar informações de documentos do Totvs Datasul MLA. Neste exemplo, completamos as informações de pendências específicas do MLA para pedidos de venda (`mla-doc-pend-aprov.cod-tip-doc = 507`).

Pré-requisitos para testar esta customização:

* Totvs Datasul versão 12 instalado (base de testes);
* Datasul MLA configurado, com documento específico para aprovação de pedido de venda;
* Aprovador Conector, para configuração da integração com o Aprovador;

Acesse o [Portal do Desenvolvedor Aprovador](https://aprovador.com/documentacao/) para saber mais sobre as customizações Progress através do Aprovador Conector.

# Aprovador

Para saber mais sobre o Aprovador, acesse [https://aprovador.com](https://aprovador.com). Para saber como customizar o Aprovador, acesse o [Portal do Desenvolvedor](https://aprovador.com/documentacao/).

# Licença

Os fontes disponíveis neste repositório são licenciados sob a [Licença MIT](https://opensource.org/licenses/MIT).

---

_Totvs Datasul é uma marca registrada da Totvs S.A_.